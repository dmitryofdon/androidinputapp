#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMouseEvent>
#include <QDebug>
#include <QInputDialog>

#include "AdbScripts//adbscriptstorage.h"
#include "Utils/keybinder.h"

#include <QAction>
#include <QDir>

MainWindow::MainWindow(QSettings &settigs, QWidget *parent):
    QMainWindow(parent),
    mUi(new Ui::MainWindow),
    mIsLongKeyPress(false),
    mIsNumLocked(false),
    mSettigs(new SettingsManager(this, settigs)),
    mPopupInformer(new PopupMessage()),
    mInternalScripts(new InternalScriptsStorage(mSettigs)),
    mExternalScripts(new ExternalScriptsStorage(mSettigs, "scripts")),
    mAdb(new AdbHandler(mSettigs)),
    mSkyScripts(new SkyAdbScripts(mSettigs))
{
    mUi->setupUi(this);
    mAdb->configureAdbPath("locate platform-tools/adb");

    mButtonBindings = KeyBinder::bindAndroidInputToQtKeys();
    mMenuActionMain.reset(new MainMenuAction(mUi->menuMenu,
                                       mInternalScripts,
                                       mExternalScripts,
                                       mPopupInformer));

    mMenuActionHelp.reset(new HelpMenuAction(mUi->menuHelp));
    mMenuActionSettings.reset(new SettingsMenuAction(mUi->menuSettings));

    mBackgroundUpdater.reset(new BackgroundUpdater(mUi->centralwidget, mAdb));
    mConnectionKeeper.reset(new ConnectionKeeper(mSettigs, mInternalScripts, this));
    mDialogManager.reset(new DialogManager(this));

    mKeyActions.insert(Qt::Key_F1, &MainWindow::reinstallAsService);
    mKeyActions.insert(Qt::Key_F2, &MainWindow::reinstallSkyApp);
    mKeyActions.insert(Qt::Key_F9, &MainWindow::connectAndConfigure);
    mKeyActions.insert(Qt::Key_F12, &MainWindow::rebootTheBoard);
    mKeyActions.insert(Qt::Key_Delete, &MainWindow::uninstallPackage);
    mKeyActions.insert(Qt::Key_Insert, &MainWindow::installPackage);
    mKeyActions.insert(Qt::Key_QuoteLeft, &MainWindow::inputText);

    connect(&mWatcher, SIGNAL(finished()), this, SLOT(actionResultChecker()));
}

MainWindow::~MainWindow()
{
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    QString inputCmd = "";
    if (event->button() == Qt::RightButton) {
        inputCmd = "keyevent KEYCODE_BACK";
    } else if (event->button() == Qt::MiddleButton) {
        inputCmd = "keyevent KEYCODE_MENU";
    } else if (event->button() == Qt::LeftButton) {
        double adbX = (1920.0 * static_cast<double>(event->x())) / this->width() ;
        double adbY = (1080.0 * static_cast<double>(event->y())) / this->height();
        inputCmd = "tap " + (QString::number(adbX)).append(" ").append(QString::number(adbY));
    }

    if (inputCmd.length() > 0) {
        if (mIsLongKeyPress) {
            inputCmd += " --longpress";
        }
        mAdb->sendShellInputCmd(inputCmd);
    }
}

void MainWindow::connectAndConfigure()
{
    QString boardIP = mSettigs->readWithDialog(SettingsManager::MOZART_GROUP_NAME, SettingsManager::BOARD_IP, true);
    if (!boardIP.isEmpty() && boardIP != "USB") {
        std::function<QString(QString)> func = [this](QString boardIP) {
            mSkyScripts->adb()->sendAdbCommand("disconnect");
            QString result = mSkyScripts->adb()->sendAdbCommand("-s " + mSkyScripts->adb()->getIP() + ":5555 root");
            qDebug() << result;
            if (!ExecScript::isResultOk(result)) {
                return result;
            }

            setWindowTitle("Connected to " + boardIP);
            return ExecScript::goodResult();
        };
        perfomAction("Connecting to the board", std::bind(func, boardIP));
    }
}

void MainWindow::reinstallAsService()
{
    QString comediaPath = mSettigs->readWithDialog(SettingsManager::MOZART_GROUP_NAME, SettingsManager::COMEDIA_PATH);
    if (comediaPath.length() > 0) {
        std::function<QString(QString)> func = [this](QString path) { return mSkyScripts->reinstallAsService(path); };
        perfomAction("Reinstalling and starting QtRestService", std::bind(func, comediaPath));
    }
}

void MainWindow::reinstallSkyApp()
{
    QString mozartPath = mSettigs->readWithDialog(SettingsManager::MOZART_GROUP_NAME, SettingsManager::MOZART_EPG_PATH);
    if (mozartPath.length() > 0) {
        std::function<QString(QString)> func = [this](QString path) { return mSkyScripts->reinstallSkyApp(path); };
        perfomAction("Reinstalling com.sky.epg", std::bind(func, mozartPath));
    }
}

void MainWindow::rebootTheBoard()
{
    std::function<QString()> func = [this]() { return mAdb->sendAdbCommand("reboot"); };
    perfomAction("Rebooting board", std::bind(func));
}

void MainWindow::uninstallPackage()
{    
    QString resul = mSkyScripts->adb()->sendAdbCommand("shell pm list packages -f");
    QStringList installedPackages = resul.split(" ");
    QString selectedApp = mDialogManager->getItem("Uninstall: ", installedPackages);
    if (!selectedApp.isEmpty()) {
        std::function<QString(QString)> func = [this](QString text) { return mAdb->sendAdbCommand("uninstall " + text); };
        perfomAction("Uninstall the app " + selectedApp, std::bind(func, selectedApp));
    }
}

void MainWindow::installPackage()
{
    QString filePath = mDialogManager->callUserGetFilePath("Install", "");
    if (!filePath.isEmpty()) {
        std::function<QString(QString)> func = [this](QString filePath) { return mAdb->sendAdbCommand("install -r " + filePath); };
        perfomAction("Installing the application " + filePath, std::bind(func, QDir::toNativeSeparators(filePath)));
    }
}

void MainWindow::inputText()
{
    QString inputText = mDialogManager->callUserDialog("Input text", "");
    qDebug() << inputText;
    if (inputText.length() > 0) {
        mAdb->sendShellInputCmd("text " + inputText);
    }
}

void MainWindow::sendPressedKey(QKeyEvent *event)
{
    QString keyevent = "";

    //qDebug() << event->key();

    switch (event->key()) {
    case Qt::Key_Pause:
        mBackgroundUpdater->revertState();
        break;
    case Qt::Key_CapsLock:
        mIsLongKeyPress ^= 1;
        qDebug() << "Long press is " << mIsLongKeyPress;
        break;
    case Qt::Key_Escape:
        keyevent = "KEYCODE_ESCAPE";
        break;
    case Qt::Key_Backspace:
        keyevent = "KEYCODE_DEL";
        break;
    case Qt::Key_Enter:
        keyevent = "KEYCODE_ENTER";
        break;
    case Qt::Key_NumLock:
        mIsNumLocked ^= true;
        break;        
    default:
        QMap<int, QString>::iterator it = mButtonBindings.find(event->key());
        if (it != mButtonBindings.end()) {
            qDebug() << "Pressed " << it.key() << " " << it.value();
            if ((it.key() >= Qt::Key_A && it.key() <= Qt::Key_ssharp) ||
                    it.key() == Qt::Key_Space) {
                //TODO: Doesn't work by sendevent. SLOW.
                keyevent = QKeySequence(it.key()).toString().toUpper();
            } else if ( mIsNumLocked && (it.key() >= Qt::Key_Left && it.key() <= Qt::Key_Down)) {
                keyevent = "KEYCODE_DPAD_" + QKeySequence(event->key()).toString().toUpper();
            } else if ( mIsNumLocked && (it.key() >= Qt::Key_0 && it.key() <= Qt::Key_9)) {
                keyevent = "KEYCODE_" + QKeySequence(event->key()).toString().toUpper();
            } else {
                std::shared_ptr<ExecScript> script = mInternalScripts->getSendEventScript();
                if (script != nullptr) {
                    script->exec(QStringList() << it.value() << QString::number(mIsLongKeyPress));
                }
            }
        }
        break;
    }

    if (keyevent.length() > 0) {
        if (mIsLongKeyPress) {
            keyevent += " --longpress";
        }
        mAdb->sendShellInputCmd("keyevent " + keyevent);
    }
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    auto item = mKeyActions.find(event->key());
    if (item != mKeyActions.end()) {
        (this->*item.value())();
    } else {
        sendPressedKey(event);
    }

}

void MainWindow::perfomAction(QString msg, std::function<QString()> func)
{
    mPopupInformer->showWaitingMsg(msg);
    QFuture<QString> action = QtConcurrent::run(func);
    mWatcher.setFuture(action);
}

void MainWindow::actionResultChecker()
{
    mPopupInformer->showScriptMsg(mWatcher.future().result());
}
