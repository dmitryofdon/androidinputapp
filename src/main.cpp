#include "mainwindow.h"
#include <QApplication>
#include <QIcon>

#include "runguard.h"

int main(int argc, char *argv[])
{    
    QSettings settings("AndroidInputApp.conf", QSettings::IniFormat);

    QApplication a(argc, argv);
    MainWindow w(settings, nullptr);
    w.setWindowIcon(QIcon(":/skyIcon.icon"));
    w.setWindowTitle("Android Input App");

    w.show();

    return a.exec();
}
