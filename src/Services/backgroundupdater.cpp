#include "backgroundupdater.h"

BackgroundUpdater::BackgroundUpdater(
        QWidget *widgetToUpdate, std::shared_ptr<AdbHandler> adb, int interval):
    mWidgetToUpdate(widgetToUpdate),
    mAdb(adb),
    mIsConnected(false),
    mIsStarted(true)
{
    mUpdateScreenTimer = new QTimer();
    mUpdateScreenTimer->setInterval(interval);
    connect(mUpdateScreenTimer, SIGNAL(timeout()), this, SLOT(updateScreen()));
    mUpdateScreenTimer->start();
}

BackgroundUpdater::~BackgroundUpdater()
{
    pause();
    delete mUpdateScreenTimer;
}

bool BackgroundUpdater::pause()
{
    mUpdateScreenTimer->stop();
    return mIsStarted = false;
}

bool BackgroundUpdater::start()
{
    mUpdateScreenTimer->start();
    return mIsStarted = true;
}

bool BackgroundUpdater::revertState()
{
    if (mIsStarted) {
        return pause();
    }
    return start();
}

void BackgroundUpdater::updateScreen()
{
    QString target_path = "/data/local/tmp/screen.png";
    QString host_path = "/tmp/screen_" + mAdb->getIP() + ".png";

    if (mScreenshotsThread.isFinished()) {
        mScreenshotsThread = QtConcurrent::run([this, host_path, target_path] {
            QString result = mAdb->sendAdbCommand("shell screencap " + target_path);
            if (result.contains("no devices/emulators found")) {
                return result;
            }
            mImgFileAccess.lock();
            result = mAdb->sendAdbCommand("pull " + target_path + " " + host_path);
            mImgFileAccess.unlock();
            if (result.contains("no devices/emulators found")) {
                return result;
            }
            return AdbHandler::SUCCESS;
        });
    }

    if (mImgFileAccess.tryLock()) {
        mWidgetToUpdate->setStyleSheet("border-image: url(" + host_path + ") 0 0 0 0 stretch stretch");
        mImgFileAccess.unlock();
    }

}
