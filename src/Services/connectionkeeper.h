#ifndef CONNECTIONKEEPER_H
#define CONNECTIONKEEPER_H

#include <QObject>
#include <QTimer>
#include <QSettings>
#include <QMainWindow>
#include <QFuture>

#include "../AdbScripts/internalscriptsstorage.h"
#include "../AdbScripts/adbexecscript.h"
#include "../AdbScripts/skyadbscripts.h"

#include "../Utils/settingsmanager.h"

class ConnectionKeeper:
        public QObject
{
    Q_OBJECT
public:
    ConnectionKeeper(std::shared_ptr<SettingsManager> settings,
            std::shared_ptr<InternalScriptsStorage> internalScripts,
                     QMainWindow *window = NULL);
    virtual ~ConnectionKeeper();

    void reconnect();

    bool isConnected() const;

    QString pollDeviceList();

private slots:
    void polling();

private:
    std::shared_ptr<SettingsManager> mSettings;
    std::shared_ptr<InternalScriptsStorage> mInternalScripts;
    QMainWindow *mMainWindow;

    std::shared_ptr<QTimer> mConnectionPolling;
    std::shared_ptr<SkyAdbScripts> mSkyScripts;

    QFuture<QString> mConnectAsRootThread;
    bool mIsConnected;
};

#endif // CONNECTIONKEEPER_H
