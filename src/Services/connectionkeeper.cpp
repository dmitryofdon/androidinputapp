#include "connectionkeeper.h"

#include <QtConcurrentRun>

ConnectionKeeper::ConnectionKeeper(std::shared_ptr<SettingsManager> settings,
        std::shared_ptr<InternalScriptsStorage> internalScripts, QMainWindow *window):
    mSettings(settings),
    mInternalScripts(internalScripts),
    mMainWindow(window),
    mConnectionPolling(new QTimer()),
    mSkyScripts(new SkyAdbScripts(settings)),
    mIsConnected(false)
{     
    mConnectionPolling->setInterval(500);
    connect(mConnectionPolling.get(), SIGNAL(timeout()),
            this, SLOT(polling()));
    mConnectionPolling->start();

    mConnectAsRootThread = QFuture<QString>();
}

ConnectionKeeper::~ConnectionKeeper()
{
}

QString ConnectionKeeper::pollDeviceList()
{
    QString boardIp = mSettings->readBoardIp().trimmed();
    QVector< std::shared_ptr<AdbDevice> > connectedDevices = mSkyScripts->readConnectedAdbDevices();
    mIsConnected = false;
    bool isUSBDevice = boardIp.toUpper() == "USB" || boardIp == "";
    AdbDevice::Type selectedDevice = isUSBDevice ? AdbDevice::Type::USB : AdbDevice::Type::Ethernet;
    for (auto device: connectedDevices) {
        if (device->type() != selectedDevice) {
            mSkyScripts->adbDisconnectDevice(device->name());
        } else {
            if (device->name() == boardIp) {
                mIsConnected = true;
            } else if (device->type() == AdbDevice::Type::USB) {
                mIsConnected = true;
            }
        }
    }

    return isUSBDevice ? "USB" : boardIp;
}

void ConnectionKeeper::polling()
{
    QString boardIp = pollDeviceList();

    if (!mIsConnected && mConnectAsRootThread.isFinished()) {
        mConnectAsRootThread = QtConcurrent::run([this]() {
            auto adb = mSkyScripts->adb();
            QString result = adb->sendAdbCommand("connect " + adb->getIP());
            if (!ExecScript::isResultOk(result)) {
                return result;
            }
            return ExecScript::goodResult();
        });
    }

    if (mMainWindow != nullptr) {
        QString newTitle = mIsConnected ? "Connected to " : "Trying to connect to ";
        newTitle += boardIp;
        mMainWindow->setWindowTitle(newTitle);
    }
}

bool ConnectionKeeper::isConnected() const
{
    return mIsConnected;
}
