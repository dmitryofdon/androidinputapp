#ifndef BACKGROUNDUPDATER_H
#define BACKGROUNDUPDATER_H

#include <QObject>
#include <QWidget>
#include <QTimer>
#include <QtConcurrentRun>
#include <QString>
#include <QMutex>

#include "../AdbScripts/adbhandler.h"

class BackgroundUpdater:
        public QObject
{
    Q_OBJECT
public:
    BackgroundUpdater(QWidget *widgetToUpdate, std::shared_ptr<AdbHandler>, int interval = 500);
    virtual ~BackgroundUpdater();

    bool start();
    bool pause();
    bool revertState();
private slots:
    void updateScreen();
private:
    QWidget *mWidgetToUpdate;
    std::shared_ptr<AdbHandler> mAdb;
    bool mIsConnected;
    QTimer *mUpdateScreenTimer;
    QFuture<QString> mScreenshotsThread;
    QMutex mImgFileAccess;
    bool mIsStarted;
};

#endif // BACKGROUNDUPDATER_H
