#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QTimer>
#include <QSettings>
#include <functional>
#include <QFutureWatcher>
#include <memory>

#include "AdbScripts/scriptfile.h"
#include "AdbScripts/adbhandler.h"
#include "AdbScripts/internalscriptsstorage.h"
#include "AdbScripts/externalscriptsstorage.h"
#include "AdbScripts/skyadbscripts.h"

#include "Actions/popupmessage.h"
#include "Actions/helpmenuaction.h"
#include "Actions/mainmenuaction.h"
#include "Actions/settingsmenuaction.h"
#include "Actions/dialogmanager.h"

#include "Utils/keybinder.h"
#include "Utils/settingsmanager.h"

#include "Services/backgroundupdater.h"
#include "Services/connectionkeeper.h"

namespace Ui {
class MainWindow;
}

class MainWindow :
        public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QSettings &settigs, QWidget *parent = 0);
    virtual ~MainWindow();

private:

    void connectAndConfigure();                    
    void reinstallAsService();    
    void reinstallSkyApp();    
    void rebootTheBoard();    
    void uninstallPackage();
    void installPackage();
    void inputText();
    
    void sendPressedKey(QKeyEvent *event);

    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;

    void perfomAction(QString msg, std::function<QString()> func);

private:
    std::shared_ptr<Ui::MainWindow> mUi;

    KeyBinder::QtAndroidBindingKeysType mButtonBindings;
    QFutureWatcher<QString> mWatcher;

    bool mIsLongKeyPress;
    bool mIsNumLocked;

    std::shared_ptr<SettingsManager> mSettigs;
    std::shared_ptr<PopupMessage> mPopupInformer;
    std::shared_ptr<InternalScriptsStorage> mInternalScripts;
    std::shared_ptr<ExternalScriptsStorage> mExternalScripts;
    std::shared_ptr<AdbHandler> mAdb;
    std::shared_ptr<BackgroundUpdater> mBackgroundUpdater;
    std::shared_ptr<SkyAdbScripts> mSkyScripts;
    std::shared_ptr<HelpMenuAction> mMenuActionHelp;
    std::shared_ptr<MainMenuAction> mMenuActionMain;
    std::shared_ptr<SettingsMenuAction> mMenuActionSettings;
    std::shared_ptr<DialogManager> mDialogManager;

    std::shared_ptr<ConnectionKeeper> mConnectionKeeper;

    typedef void(MainWindow::*KeyActionFunction)();
    QMap<int, KeyActionFunction> mKeyActions;

private slots:
    void actionResultChecker();
};

#endif // MAINWINDOW_H
