#ifndef EXTERNALSCRIPTSSTORAGE_H
#define EXTERNALSCRIPTSSTORAGE_H

#include "adbscriptstorage.h"
#include <QString>
#include <QStringList>

class ExternalScriptsStorage:
        public AdbScriptsStorage
{
public:
    ExternalScriptsStorage(std::shared_ptr<SettingsManager> settingsManager, QString path);

    virtual QStringList getScriptList();

    QVector<QString> getScripts();
    void runScript(QString name);

private:
    bool isScriptExist(QVector<QString> scripts, QString name);

private:
    QString mPath;
};

#endif // EXTERNALSCRIPTSSTORAGE_H
