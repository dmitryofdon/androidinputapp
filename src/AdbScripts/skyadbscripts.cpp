#include "skyadbscripts.h"

#include <QRegExp>

SkyAdbScripts::SkyAdbScripts(std::shared_ptr<SettingsManager> settingsManager):
    mSettingsManager(settingsManager),
    mAdb(new AdbHandler(settingsManager))
{

}

SkyAdbScripts::~SkyAdbScripts()
{
    clearConnectedAdbDevices();
}

QString SkyAdbScripts::reinstallSkyApp(QString mozartPath)
{
    mAdb->sendAdbCommand("uninstall com.sky.epg");
    QString result = mAdb->sendAdbCommand("install -r " + mozartPath +
                         mSettingsManager->read(SettingsManager::BUILD_PATH_GROUP_NAME, SettingsManager::EPG_UI_BUILD_PATH));
    if (result.contains("Success")) {
        return AdbHandler::SUCCESS;
    }
    return result;
}

QString SkyAdbScripts::reinstallAsService(QString comediaPath)
{
    mAdb->sendAdbCommand("uninstall com.iwedia.comedia.qtrestservice");
    mAdb->sendAdbCommand("install -r " + comediaPath +
                         mSettingsManager->read(SettingsManager::BUILD_PATH_GROUP_NAME, SettingsManager::AS_BUILD_PATH));
    QString result = mAdb->sendAdbCommand("shell am broadcast -a com.iwedia.comedia.qtrestservice.START -n com.iwedia.comedia.qtrestservice/.QTRestServiceReceiver");
    if (result.contains("Broadcast completed: result=0")) {
        return AdbHandler::SUCCESS;
    }
    return result;
}

QString SkyAdbScripts::adbDisconnectDevice(QString ip)
{
    return mAdb->sendAdbCommand("disconnect " + ip);
}

void SkyAdbScripts::clearConnectedAdbDevices()
{
    mConnectedDevices.clear();
}

QVector< std::shared_ptr<AdbDevice> > SkyAdbScripts::readConnectedAdbDevices()
{
    clearConnectedAdbDevices();
    QString result = mAdb->sendAdbCommand("devices");
    if (result.contains("List of devices attached")) {
        QStringList resultSplit = result.split("\n");
        for (QString item : resultSplit) {
            std::shared_ptr<AdbDevice> device = AdbDevice::create(item);
            if (device != nullptr) {
                mConnectedDevices.push_back(device);
            }
        }
    }
    return mConnectedDevices;
}
std::shared_ptr<AdbHandler> SkyAdbScripts::adb()
{
    return mAdb;
}



