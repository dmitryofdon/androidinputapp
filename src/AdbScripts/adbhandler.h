#ifndef ADBHANDLER_H
#define ADBHANDLER_H

#include <QString>
#include <QStringList>
#include <memory>

#include "scriptfile.h"
#include "../Utils/settingsmanager.h"

class ExecScript;
class AdbHandler
{
public:
    static QString SUCCESS;

    AdbHandler(std::shared_ptr<SettingsManager> settingsManager);

    void configureAdbPath(QString whichAdbScript);

    QString createAdbCommand(QString cmd);
    QString sendAdbCommand(QString cmd);

    QString sendShellCommandCmd(QString cmd);
    QString sendShellInputCmd(QString cmd);
    QString sendShellCmd(QString cmd);
    QString sendSendEventCmd(QString arg1, QString arg2, QString arg3, QString event = "0");

    QString getPath() { return mSettingsManager->readAdbPath(); }
    QString getIP() { return mSettingsManager->readBoardIp(); }

    QString executeScriptAdb(const ScriptFile &script, const QStringList &args);
    QString executeScript(const ScriptFile &script, QStringList args = QStringList());

    static QString startProcess(const QString &cmd, QStringList args = QStringList());

private:

    std::shared_ptr<SettingsManager> mSettingsManager;
};

#endif // ADBHANDLER_H
