#include "adbscriptstorage.h"

#include <QProcess>
#include <QMap>
#include <QDir>

AdbScriptsStorage::AdbScriptsStorage(std::shared_ptr<SettingsManager> settingsManager):
    mSettingsManager(settingsManager)
{

}

AdbScriptsStorage::~AdbScriptsStorage() {
}

QVector<QString> AdbScriptsStorage::scanScripts(QString dirPathFrom)
{
    mScanningPath = dirPathFrom;
    foreach(const QString &fileName, QDir(dirPathFrom).entryList()) {
        if (fileName.contains(".sh")) {
            mScannedScriptNames.push_back(fileName);
        }
    }
    return mScannedScriptNames;
}

void AdbScriptsStorage::createScannedScripts() {
    foreach (QString fileName, mScannedScriptNames) {
        mAdbScripts.insert(fileName, createScriptFromFile(mScanningPath, fileName));
    }
}

QString AdbScriptsStorage::execScript(QString scriptFileName, QStringList args)
{
    std::shared_ptr<ExecScript> scriptFile = findCreatedScript(scriptFileName);
    return scriptFile->exec(args);
}

std::shared_ptr<AdbScript> AdbScriptsStorage::findCreatedScript(QString name) {
    auto item = mAdbScripts.find(name);
    if (item != mAdbScripts.end()) {
        return item.value();
    }
    return nullptr;
}

std::shared_ptr<AdbScript> AdbScriptsStorage::createScriptFromFile(QString dirPathFrom, QString fileName) {
    QString scriptName = fileName;
    std::shared_ptr<AdbScript> script(new AdbScript(mSettingsManager, fileName));
    QFile file(dirPathFrom + scriptName);
    QByteArray resourcesFileData;
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        resourcesFileData = file.readAll();
        file.close();
    }
    script->addData(resourcesFileData);
    return script;
}
