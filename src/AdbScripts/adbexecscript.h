#ifndef ADBEXECSCRIPT_H
#define ADBEXECSCRIPT_H

#include <QString>

#include "adbhandler.h"
#include "scriptfile.h"

class ExecScript
{
public:
    virtual QString exec(QStringList args = QStringList()) = 0;
    virtual QString execNoAdb(QStringList args = QStringList()) = 0;
    virtual ~ExecScript();

    static bool isResultOk(QString result) {
        return result.endsWith(goodResult()) ||
                result.startsWith("Success") ||
                result.trimmed() == "";
    }

    static QString goodResult() { return AdbHandler::SUCCESS; }
};

class AdbScript:
        public ScriptFile,
        public ExecScript
{
public:
    AdbScript(std::shared_ptr<SettingsManager> settingsManager, QString fileName);
    virtual ~AdbScript();

    virtual QString exec(QStringList args = QStringList());
    virtual QString execNoAdb(QStringList args = QStringList());

private:
    std::shared_ptr<SettingsManager> mSettingsManager;
    std::shared_ptr<AdbHandler> mAdb;
};

#endif // ADBEXECSCRIPT_H
