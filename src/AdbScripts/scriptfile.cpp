#include "scriptfile.h"
#include <QTextStream>
#include <QProcess>
#include <QDir>
#include <QDebug>
#include <QApplication>

QString ScriptFile::SCRIPT_FOLDER_NAME = "utils";

void ScriptFile::setPermissions()
{
    mScript->setPermissions(mScript->permissions() | QFile::ExeGroup | QFile::ExeOther | QFile::ExeOther | QFile::ExeUser);
}

ScriptFile::ScriptFile(QString fileName):
    mFileName(fileName),
    mFilePath(QCoreApplication::applicationDirPath() + "/" + SCRIPT_FOLDER_NAME + "/" + mFileName)
{    
    if (!QDir(SCRIPT_FOLDER_NAME).exists()) {
        QDir().mkdir(SCRIPT_FOLDER_NAME);
    }
    removeFile(mFilePath);
    mScript.reset(new QFile(mFilePath));
    setPermissions();
}

void ScriptFile::removeFile(QString pathToFile)
{
    QProcess process;
    process.execute("rm -f " + pathToFile);
    process.waitForFinished();
}

ScriptFile::~ScriptFile()
{
    removeFile(mFilePath);
    if (QDir(SCRIPT_FOLDER_NAME).exists()) {
        QDir().rmdir(SCRIPT_FOLDER_NAME);
    }
}

void ScriptFile::addLineLn(QString text)
{
    if (mScript->open(QIODevice::WriteOnly | QIODevice::Text |
                         QIODevice::Append)) {
        QTextStream stream(mScript.get());
        stream << text << "\n";
        mScript->close();
        setPermissions();
    }
}

void ScriptFile::addData(QByteArray bytes) {
    if (mScript->open(QIODevice::WriteOnly | QIODevice::Text |
                         QIODevice::Append)) {
        QTextStream stream(mScript.get());
        stream << bytes;
        mScript->close();
        setPermissions();
    }
}
