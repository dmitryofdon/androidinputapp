#include "adbexecscript.h"

#include <QStringList>

ExecScript::~ExecScript() {

}

AdbScript::AdbScript(std::shared_ptr<SettingsManager> settingsManager, QString fileName):
    ScriptFile(fileName),
    mSettingsManager(settingsManager),
    mAdb(new AdbHandler(settingsManager))
{

}

AdbScript::~AdbScript()
{
}

QString AdbScript::exec(QStringList args)
{
    return mAdb->executeScriptAdb(*this, args);
}

QString AdbScript::execNoAdb(QStringList args)
{
    return mAdb->executeScript(*this, args);
}
