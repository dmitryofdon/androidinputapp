#include "adbhandler.h"
#include "../AdbScripts/internalscriptsstorage.h"

#include <QDebug>
#include <QProcess>

QString AdbHandler::SUCCESS = "SCRIPT_LAUNCHED_SUCCESSFULLY\n";

AdbHandler::AdbHandler(std::shared_ptr<SettingsManager> settingsManager):
    mSettingsManager(settingsManager)
{

}

void AdbHandler::configureAdbPath(QString whichAdbScript)
{
    QString storedAdbPath = mSettingsManager->read(
                SettingsManager::ADB_GROUP_NAME, SettingsManager::ADB_PATH);
    if (storedAdbPath.length() == 0) {
        QString foundAdbPath = startProcess(whichAdbScript);
        QStringList pathSplit = foundAdbPath.split("\n");
        if (pathSplit.size() > 1) {
            foundAdbPath = pathSplit.at(0);
        } else {
            foundAdbPath = "";
        }
        mSettingsManager->writeDefault(
                    SettingsManager::ADB_GROUP_NAME, SettingsManager::ADB_PATH, foundAdbPath);
    }
    while(storedAdbPath.length() == 0) {
        storedAdbPath = mSettingsManager->readAdbPath();
    };
}

QString AdbHandler::createAdbCommand(QString cmd)
{
    return QString(getPath() + " -s " + getIP() + ":5555 " + cmd + "\n");
}

QString AdbHandler::sendAdbCommand(QString cmd)
{
    QString fullcmd = createAdbCommand(cmd);
    return startProcess(fullcmd.toLatin1().data());
}

QString AdbHandler::sendShellCmd(QString cmd)
{
    return sendAdbCommand("shell " + cmd);
}

QString AdbHandler::sendShellCommandCmd(QString cmd)
{
    return sendAdbCommand("shell command " + cmd);
}

QString AdbHandler::sendShellInputCmd(QString cmd)
{
    return sendAdbCommand("shell input " + cmd);
}

QString AdbHandler::sendSendEventCmd(QString arg1, QString arg2, QString arg3, QString event)
{
    return sendAdbCommand("shell sendevent /dev/input/event" + event + " " + arg1 + " " + arg2 + " " + arg3);
}

QString AdbHandler::executeScriptAdb(const ScriptFile &script, const QStringList &args)
{
    return executeScript(script, QStringList() << getPath() << mSettingsManager->readBoardIp() << args);
}

QString AdbHandler::executeScript(const ScriptFile &script, QStringList args)
{
    return startProcess(script.getPath(), args);
}

QString AdbHandler::startProcess(const QString &cmd, QStringList args)
{   
    QProcess process;
    if (args.size() > 0) {
        qDebug() << cmd << args;
        process.start(cmd, args);
    } else {
        qDebug() << cmd;
        process.start(cmd);
    }
    process.waitForFinished();
    QString result = process.readAllStandardOutput();
    //qDebug() << result;
    return result;
}
