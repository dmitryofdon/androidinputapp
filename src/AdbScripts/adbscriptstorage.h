#ifndef ADBSCRIPTSTORAGE_H
#define ADBSCRIPTSTORAGE_H

#include "adbhandler.h"
#include "adbexecscript.h"

#include "../Utils/settingsmanager.h"

#include <QVector>
#include <QMap>
#include <memory>

class AdbScriptsStorage
{
public:    
    AdbScriptsStorage(std::shared_ptr<SettingsManager> settingsManager);
    virtual ~AdbScriptsStorage();

    QString execScript(QString scriptFileName, QStringList args = QStringList());
    virtual QStringList getScriptList() = 0;

protected:
    QVector<QString> scanScripts(QString dirPathFrom);
    void createScannedScripts();
    std::shared_ptr<AdbScript> createScriptFromFile(QString dirPathFrom, QString fileName);
    std::shared_ptr<AdbScript> findCreatedScript(QString name);
    QVector<QString> getScannedScripts() { return mScannedScriptNames; }

protected:
    QVector<QString> mScannedScriptNames;
    QMap< QString, std::shared_ptr<AdbScript> > mAdbScripts;
    std::shared_ptr<SettingsManager> mSettingsManager;
    QString mScanningPath;
};

#endif // ADBSCRIPTSTORAGE_H
