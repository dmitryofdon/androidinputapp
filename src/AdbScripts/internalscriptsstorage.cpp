#include "internalscriptsstorage.h"

QString InternalScriptsStorage::SCRIPTS_PATH = ":/scripts/";

InternalScriptsStorage::InternalScriptsStorage(std::shared_ptr<SettingsManager> settingsManager):
    AdbScriptsStorage(settingsManager)
{
    scanScripts(SCRIPTS_PATH);
    createScannedScripts();
}

QStringList InternalScriptsStorage::getScriptList() {
    QStringList scripts;
    foreach (QString fileName, mScannedScriptNames) {
        if (!fileName.startsWith("system_")) {
            scripts << fileName;
        }
    }
    return scripts;
}

std::shared_ptr<ExecScript> InternalScriptsStorage::getSendEventScript()
{
    return findCreatedScript("system_send_input_event.sh");
}

InternalScriptsStorage::~InternalScriptsStorage()
{

}
