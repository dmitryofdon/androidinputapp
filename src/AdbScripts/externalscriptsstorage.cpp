#include "externalscriptsstorage.h"

#include <QDebug>
#include <QProcess>
#include <QMessageBox>

ExternalScriptsStorage::ExternalScriptsStorage(std::shared_ptr<SettingsManager> settingsManager, QString path):
    AdbScriptsStorage(settingsManager),
    mPath(path)
{

}

QStringList ExternalScriptsStorage::getScriptList()
{
    QVector<QString> v = getScripts();
    QStringList list;
    for (auto item : v) {
        list << item;
    }
    return list;
}

QVector<QString> ExternalScriptsStorage::getScripts()
{
    return scanScripts(mPath);
}

bool ExternalScriptsStorage::isScriptExist(QVector<QString> scripts, QString name)
{
    for (QString script : scripts) {
        if (script == name) {
            return true;
        }
    }
    return false;
}

void ExternalScriptsStorage::runScript(QString name)
{
    QString result = "";
    QVector<QString> scripts = scanScripts(mPath);
    if (!isScriptExist(scripts, name)) {
        result = "Error! Script was deleted.";
    } else {
        result = AdbHandler::startProcess(mPath + "/" + name);
    }

    QMessageBox msg;
    msg.setStandardButtons(QMessageBox::Ok);
    msg.setInformativeText("Results");
    msg.setText(result);
    msg.show();
}
