#ifndef INTERNALSCRIPTSSTORAGE_H
#define INTERNALSCRIPTSSTORAGE_H

#include "adbscriptstorage.h"

class InternalScriptsStorage:
        public AdbScriptsStorage
{
public:

    static QString SCRIPTS_PATH;

    InternalScriptsStorage(std::shared_ptr<SettingsManager> settingsManager);
    virtual ~InternalScriptsStorage();    

    QStringList getScriptList();
    std::shared_ptr<ExecScript> getSendEventScript();

private:
    void configureAdbPath();
};



#endif // INTERNALSCRIPTSSTORAGE_H
