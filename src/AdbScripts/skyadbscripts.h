#ifndef SKYADBSCRIPTS_H
#define SKYADBSCRIPTS_H

#include "adbhandler.h"

#include <QStringList>
#include <QRegExp>
#include <QVector>

class AdbDevice {
public:
    enum Type {
        Uknown = 0,
        Ethernet,
        USB,
    };

    static std::shared_ptr<AdbDevice> create(QString attachedReportLine) {

        QMap<Type, QRegExp> list;
        list.insert(Ethernet, ipRegExp());
        list.insert(USB, usbRegExp());
        for (auto it = list.begin(); it != list.end(); ++it) {
            QRegExp exp = it.value();
            int index = attachedReportLine.indexOf(exp);
            if (index != -1) {
                int lastIndex = exp.matchedLength();
                return std::shared_ptr<AdbDevice>(new AdbDevice(it.key(), attachedReportLine.mid(index, lastIndex)));
            }
        }
        return nullptr;
    }

    static QRegExp ipRegExp() {
        return QRegExp("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}"); }
    static QRegExp usbRegExp() {
        return QRegExp("([A-Za-z]{2})[0-9]{6}"); }

    Type type() const { return mType; }
    QString name() const { return mName; }

private:

    AdbDevice(Type devType, QString devName):
        mName(devName),
        mType(devType)
    {
        ;
    }

private:
    QString mName;
    Type mType;
};

class SkyAdbScripts
{
public:
    SkyAdbScripts(std::shared_ptr<SettingsManager> settingsManager);
    ~SkyAdbScripts();

    QString reinstallSkyApp(QString mozartPath);
    QString reinstallAsService(QString comediaPath);
    QStringList adbListOfInstalledApps();
    QString adbDisconnectDevice(QString ip);
    QVector< std::shared_ptr<AdbDevice> > readConnectedAdbDevices();
    std::shared_ptr<AdbHandler> adb();

private:
    void clearConnectedAdbDevices();

private:
    std::shared_ptr<SettingsManager>  mSettingsManager;
    std::shared_ptr<AdbHandler> mAdb;
    QVector< std::shared_ptr<AdbDevice> > mConnectedDevices;
};

#endif // SKYADBSCRIPTS_H
