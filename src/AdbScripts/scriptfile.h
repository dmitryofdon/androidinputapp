#ifndef SCRIPTFILE_H
#define SCRIPTFILE_H

#include <QString>
#include <QFile>
#include <memory>

class ScriptFile
{
public:

    static QString SCRIPT_FOLDER_NAME;

public:
    ScriptFile(QString fileName);
    virtual ~ScriptFile();

    void removeFile(QString pathToFile);

    void addLineLn(QString text);
    void addData(QByteArray bytes);

    QString getPath() const { return mFilePath; }

private:
    void setPermissions();
    
private:

    const QString &mFileName;
    QString mFilePath;
    std::shared_ptr<QFile> mScript;
};

#endif // SCRIPTFILE_H
