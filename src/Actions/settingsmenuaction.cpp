#include "settingsmenuaction.h"

#include <QDesktopServices>
#include <QUrl>

SettingsMenuAction::SettingsMenuAction(QMenu *menu, QObject *parent):
    QObject(parent),
    mMainMenu(menu)
{
    mEditAction = mMainMenu->addAction("Edit");
    connect(mEditAction, SIGNAL(triggered(bool)),
            this, SLOT(menuAction(bool)));
}

SettingsMenuAction::~SettingsMenuAction()
{
    delete mEditAction;
}

void SettingsMenuAction::menuAction(bool )
{
    QDesktopServices::openUrl(QUrl("AndroidInputApp.conf"));
}
