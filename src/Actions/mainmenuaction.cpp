#include "mainmenuaction.h"

#include "../AdbScripts/externalscriptsstorage.h"
#include "../AdbScripts/internalscriptsstorage.h"

#include <QDebug>
#include <QVector>

MainMenuAction::MainMenuAction(QMenu* menu,
                               std::shared_ptr<InternalScriptsStorage> internalScripts,
                               std::shared_ptr<ExternalScriptsStorage> externalScripts,
                               std::shared_ptr<PopupMessage> informer):
    mMainMenu(menu),
    mInternalScripts(internalScripts),
    mExternalScripts(externalScripts),
    mPopupInformer(informer),
    mUserDialog(new DialogManager())
{
    connect(mMainMenu, SIGNAL(hovered(QAction*)),
            this, SLOT(mainMenuTriggered(QAction*)));

    mInternalScriptsAct = createMenu(QString("Internal Scripts"), mInternalScripts->getScriptList());
    connect(mInternalScriptsAct->menu(), SIGNAL(triggered(QAction*)),
            this, SLOT(internalScriptsAction(QAction*)));

    mExternalScriptsAct = createMenu(QString("External Scripts"), mExternalScripts->getScriptList());
    connect(mExternalScriptsAct->menu(), SIGNAL(triggered(QAction*)),
            this, SLOT(externalScriptsAction(QAction*)));
}

MainMenuAction::~MainMenuAction()
{
    deleteMenu(mInternalScriptsAct);
    deleteMenu(mExternalScriptsAct);
}

std::shared_ptr<QAction> MainMenuAction::createMenu(QString name, QStringList items)
{
    std::shared_ptr<QAction> action(mMainMenu->addAction(name));
    action->setMenu(new QMenu);
    addActions(action->menu(), items);
    return action;
}

void MainMenuAction::deleteMenu(std::shared_ptr<QAction> action) {
    removeActions(action->menu());
    delete action->menu();
}

void MainMenuAction::addActions(QMenu* menu, QStringList items) {
    foreach (QString item, items) {
        menu->addAction(item);
    }
}

void MainMenuAction::removeActions(QMenu *menu) {
    foreach (QAction *item, menu->actions()) {
        menu->removeAction(item);
    };
}

QString MainMenuAction::restorePreviousValue(QAction *action)
{
    auto it = mUserLatestPaths.find(action->text());
    if (it != mUserLatestPaths.end()) {
        return it.value();
    }
    return QString();
}

void MainMenuAction::mainMenuTriggered(QAction *)
{
}

void MainMenuAction::storeCurrentValue(QAction *action, QString choosenPath)
{
    if (choosenPath.length() > 0) {
        mUserLatestPaths.insert(action->text(), choosenPath);
    }
}

void MainMenuAction::internalScriptsAction(QAction *action)
{
    QString choosenPath = QString();
    QString previousValue = restorePreviousValue(action);

    if (action->text() == "ota_update_img.sh") {
        choosenPath = mUserDialog->callUserGetFilePath("To File", previousValue);
        if (choosenPath.length() > 0) {
            mInternalScripts->execScript(action->text(), QStringList() << choosenPath);
        }
    } else {
        mInternalScripts->execScript(action->text());
    }

    storeCurrentValue(action, choosenPath);
}

void MainMenuAction::externalScriptsAction(QAction *action)
{
    qDebug() << action->text();

    removeActions(mExternalScriptsAct->menu());
    addActions(mExternalScriptsAct->menu(), mExternalScripts->getScriptList());
}
