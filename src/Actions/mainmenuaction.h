#ifndef MAINMENUACTION_H
#define MAINMENUACTION_H

#include <QObject>
#include <QMenu>
#include <QMap>
#include <QAction>
#include <memory>

#include "dialogmanager.h"

#include "popupmessage.h"

class ExternalScriptsStorage;
class InternalScriptsStorage;

class MainMenuAction:
        public QObject
{
    Q_OBJECT
public:
    explicit MainMenuAction(QMenu* menu,
                   std::shared_ptr<InternalScriptsStorage> internalScripts,
                   std::shared_ptr<ExternalScriptsStorage> externalScripts,
                   std::shared_ptr<PopupMessage> informer);
    virtual ~MainMenuAction();

    void storeCurrentValue(QAction *action, QString choosenPath);

private:
    QString restorePreviousValue(QAction* action);
    std::shared_ptr<QAction> createMenu(QString name, QStringList items);
    void deleteMenu(std::shared_ptr<QAction> action);

    void addActions(QMenu* menu, QStringList items);
    void removeActions(QMenu *menu);

private slots:
    void mainMenuTriggered(QAction *action);
    void internalScriptsAction(QAction *action);
    void externalScriptsAction(QAction *action);

private:
    QMenu* mMainMenu;
    std::shared_ptr<InternalScriptsStorage> mInternalScripts;
    std::shared_ptr<ExternalScriptsStorage> mExternalScripts;
    std::shared_ptr<PopupMessage> mPopupInformer;

    std::shared_ptr<QAction> mInternalScriptsAct;
    std::shared_ptr<QAction> mExternalScriptsAct;
    std::shared_ptr<DialogManager> mUserDialog;

    QMap<QString, QString> mUserLatestPaths;
};

#endif // MAINMENUACTION_H
