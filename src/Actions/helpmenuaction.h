#ifndef HelpMenuAction_H
#define HelpMenuAction_H

#include <QMessageBox>
#include <QMenu>
#include <QAction>

class HelpMenuAction:
        public QObject
{
    Q_OBJECT
public:
    HelpMenuAction(QMenu *menu, QObject *parent = NULL);
    virtual ~HelpMenuAction();

public slots:
    void actionHelp(QAction *action);

private:
    static QString getText();

private:
    static QString mAboutMenuName;

    QMessageBox *mHotkeysMessageBox;
    QString mHelpText;
};

#endif // HelpMenuAction_H
