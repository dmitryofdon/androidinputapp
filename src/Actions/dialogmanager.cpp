#include "dialogmanager.h"

#include <QInputDialog>

DialogManager::DialogManager(QWidget *parent):
    mParent(parent)
{

}

QString DialogManager::getItem(QString type, QStringList items, bool &isAccepted)
{
     return QInputDialog::getItem(mParent, type, "Select:", items, 0, false, &isAccepted);
}

QString DialogManager::getItem(QString type, QStringList items)
{
    bool isAccepted = false;
    QString selectedItem = getItem(type, items, isAccepted);
    if (!isAccepted) {
        return QString();
    }
    return selectedItem;
}

QString DialogManager::callUserDialog(QString dialogHeader, QString defValue)
{
    bool isAccepted = false;
    QString selectedItem = callUserDialog(dialogHeader, defValue, isAccepted);
    if (!isAccepted) {
        return QString();
    }
    return selectedItem;
}

QString DialogManager::callUserGetFilePath(QString dialogHeader, QString defValue)
{
    QString path = callUserDialog(dialogHeader, defValue);
    if (path.startsWith("file:///")) {
        path.replace("file:///", "/");
    }
    return path;
}

QString DialogManager::callUserDialog(QString dialogHeader, QString defValue, bool &isOk)
{
    return QInputDialog::getText(mParent, dialogHeader, dialogHeader + ":", QLineEdit::Normal, defValue, &isOk);
}
