#include "popupmessage.h"

#include <QDebug>

#include "../AdbScripts/adbexecscript.h"

PopupMessage::PopupMessage():
    mAutoHideTimer(new QTimer()),
    mShowTimer(new QTimer())
{
    setStandardButtons(QMessageBox::Ok);
    setWindowFlags(Qt::ToolTip);
    setStyleSheet("QPushButton{ max-width:20px; font-size: 8px;}");

    QObject::connect(mAutoHideTimer, SIGNAL(timeout()), this, SLOT(hideBox()));
    mAutoHideTimer->setInterval(2 * 1000);
    mAutoHideTimer->setSingleShot(true);

    QObject::connect(mShowTimer, SIGNAL(timeout()), this, SLOT(showBox()));
    mShowTimer->setInterval(1);
    mShowTimer->setSingleShot(true);
}

PopupMessage::~PopupMessage()
{
    delete mAutoHideTimer;
    delete mShowTimer;
}

void PopupMessage::showMsgBox(QString msg, QMessageBox::Icon icon, QString info)
{
    qDebug() << msg;
    setInformativeText(info);
    setIcon(icon);
    setText(msg);
    mShowTimer->start();
}

void PopupMessage::showWaitingMsg(QString msg)
{
    showMsgBox(msg, QMessageBox::Question, "Wait a second ...");
}

void PopupMessage::showScriptMsg(QString msg)
{
    if (ExecScript::isResultOk(msg)) {
        showSuccessMsg("Ok");
    } else {
        showErrorMsg(msg);
    }
}

void PopupMessage::showSuccessMsg(QString msg)
{
    showMsgBox(msg, QMessageBox::Information, "All is good");
    mAutoHideTimer->start();
}

void PopupMessage::showErrorMsg(QString msg)
{
    showMsgBox(msg, QMessageBox::Critical, "Error");
    mAutoHideTimer->start();
}

void PopupMessage::showBox()
{
    setEnabled(true);
    show();
}

void PopupMessage::hideBox()
{
    hide();
    setEnabled(false);
}
