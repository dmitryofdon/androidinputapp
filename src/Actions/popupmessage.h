#ifndef POPUPMESSAGE_H
#define POPUPMESSAGE_H

#include <QMessageBox>
#include <QTimer>

class PopupMessage:
        private QMessageBox
{
    Q_OBJECT
public:
    PopupMessage();
    virtual ~PopupMessage();
    void showMsgBox(QString msg, QMessageBox::Icon icon, QString info);
    void showSuccessMsg(QString msg);
    void showErrorMsg(QString msg);
    void showWaitingMsg(QString msg);
    void showScriptMsg(QString msg);
public slots:
    void showBox();
    void hideBox();
private:
    QTimer *mAutoHideTimer;
    QTimer *mShowTimer;
};

#endif // POPUPMESSAGE_H
