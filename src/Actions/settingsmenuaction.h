#ifndef SETTINGSMENUACTION_H
#define SETTINGSMENUACTION_H

#include <QObject>
#include <QMenu>

class SettingsMenuAction: public QObject
{
    Q_OBJECT
public:
    SettingsMenuAction(QMenu *menu, QObject *parent = NULL);
    ~SettingsMenuAction();

private slots:
    void menuAction(bool);

private:
    QMenu *mMainMenu;
    QAction *mEditAction;
};

#endif // SETTINGSMENUACTION_H
