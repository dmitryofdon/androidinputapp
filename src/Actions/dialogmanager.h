#ifndef DialogManager_H
#define DialogManager_H

#include <QStringList>

class DialogManager
{
public:
    explicit DialogManager(QWidget *parent = NULL);

    QString getItem(QString type, QStringList items, bool &isAccepted);
    QString getItem(QString type, QStringList items);

    QString callUserDialog(QString dialogHeader, QString defValue, bool &isAccepted);
    QString callUserDialog(QString dialogHeader, QString defValue);
    QString callUserGetFilePath(QString dialogHeader = "Path to file", QString defValue = "");

private:
    QWidget *mParent;
};

#endif // DialogManager_H
