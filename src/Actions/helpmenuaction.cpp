#include "helpmenuaction.h"

#include <QFile>
#include <QDebug>

QString HelpMenuAction::mAboutMenuName = "About";

HelpMenuAction::HelpMenuAction(QMenu *menu, QObject *parent):
    QObject(parent)
{
    mHotkeysMessageBox = new QMessageBox();
    mHotkeysMessageBox->setStandardButtons(QMessageBox::Ok);
    mHotkeysMessageBox->setWindowFlags(Qt::Window | Qt::Window);
    mHotkeysMessageBox->setIcon(QMessageBox::Question);
    mHotkeysMessageBox->setText(getText());

    QAction *about = menu->addAction(mAboutMenuName);
    about->setIcon(QIcon(":/about.icon"));
    connect(menu, SIGNAL(triggered(QAction*)), this, SLOT(actionHelp(QAction*)));
}

HelpMenuAction::~HelpMenuAction()
{
    delete mHotkeysMessageBox;
}

QString HelpMenuAction::getText()
{
    QFile file(":/help.txt");
    QByteArray resourcesFileData;
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        resourcesFileData = file.readAll();
        file.close();
    }
    return QString(resourcesFileData);
}

void HelpMenuAction::actionHelp(QAction *action)
{
    if (action->text() == mAboutMenuName) {
        mHotkeysMessageBox->show();
    }
}
