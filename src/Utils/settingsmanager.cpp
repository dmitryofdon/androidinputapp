#include "settingsmanager.h"

#include "../Actions/dialogmanager.h"

QString SettingsManager::ADB_GROUP_NAME = "ADB";
QString SettingsManager::MOZART_GROUP_NAME = "Mozart";
QString SettingsManager::ADB_PATH = "ADB_PATH";
QString SettingsManager::COMEDIA_PATH = "COMEDIA_PATH";
QString SettingsManager::MOZART_EPG_PATH = "MOZART_EPG_PATH";
QString SettingsManager::BOARD_IP = "BOARD_IP";

QString SettingsManager::EPG_UI_BUILD_PATH = "EPG_UI_BUILD_PATH";
QString SettingsManager::AS_BUILD_PATH = "AS_BUILD_PATH";
QString SettingsManager::BUILD_PATH_GROUP_NAME = "BUILD_PATH_GROUP_NAME";

QString SettingsManager::DEFAULT_EPG_UI_BUILD_PATH = "/source/android-build//build/outputs/apk/android-build-debug.apk";
QString SettingsManager::DEFAULT_AS_BUILD_PATH = "/jni/teatro35/applications/QtRESTService/app/build/outputs/apk/debug/app-debug.apk";

SettingsManager::SettingsManager(QWidget *parent, QSettings &settings):
    mParent(parent),
    mSettigs(settings)
{
    writeDefault(SettingsManager::BUILD_PATH_GROUP_NAME,
                           SettingsManager::EPG_UI_BUILD_PATH,
                           SettingsManager::DEFAULT_EPG_UI_BUILD_PATH);
    writeDefault(SettingsManager::BUILD_PATH_GROUP_NAME,
                           SettingsManager::AS_BUILD_PATH,
                           SettingsManager::DEFAULT_AS_BUILD_PATH);
}

QString SettingsManager::readWithDialog(QString group, QString name, bool isAwaysDialog)
{
    DialogManager dialog(mParent);
    QString value = safetyRead(group, name);
    if (value.length() == 0 || isAwaysDialog) {
        bool isAccepted = false;
        QString newValue = dialog.callUserDialog(name, value, isAccepted);
        if (!isAccepted) {
            return "";
        }
        if (!newValue.isEmpty()) {
            safetySet(group, name, newValue);
            return newValue;
        }
    }
    return value;
}

QString SettingsManager::read(QString group, QString name)
{
    return safetyRead(group, name);
}

void SettingsManager::writeDefault(QString group, QString name, QString defaultValue)
{
    QString value = safetyRead(group, name);
    if (value.length() == 0) {
        safetySet(group, name, defaultValue);
    }
}

QString SettingsManager::readAdbPath()
{
    return readWithDialog(SettingsManager::ADB_GROUP_NAME, SettingsManager::ADB_PATH);
}

QString SettingsManager::readBoardIp()
{
    return readWithDialog(SettingsManager::MOZART_GROUP_NAME, SettingsManager::BOARD_IP);
}

QString SettingsManager::safetyRead(QString group, QString name)
{
    safetyBeginGroup(group);
    QString value = mSettigs.value(name).toString();
    safetyEndGroup();
    return value;
}

void SettingsManager::safetySet(QString group, QString name, QString value)
{
    safetyBeginGroup(group);
    mSettigs.setValue(name, value);
    safetyEndGroup();
}

void SettingsManager::safetyBeginGroup(QString group)
{
    mDataAccess.lock();
    mSettigs.beginGroup(group);
}

void SettingsManager::safetyEndGroup()
{
    mSettigs.endGroup();
    mDataAccess.unlock();
}
