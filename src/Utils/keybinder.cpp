#include "keybinder.h"

#include <QDebug>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QKeySequence>

KeyBinder::KeyBinder()
{

}

KeyBinder::QtAndroidBindingKeysType KeyBinder::bindAndroidInputToQtKeys()
{
    AndroidInputKeysType androidKeys = readAndroidInputKeys();

    static QtAndroidBindingKeysType qtAndroidBindings;
    if (qtAndroidBindings.size() == 0) {
        int bindingsCount = KeyBinder::bindQtKeys(Qt::Key_Escape, Qt::Key_LaunchH, androidKeys, qtAndroidBindings);
        bindingsCount += KeyBinder::bindQtKeys(Qt::Key_Space, Qt::Key_ydiaeresis, androidKeys, qtAndroidBindings);
        bindingsCount += KeyBinder::bindQtKeysCustom(qtAndroidBindings);
        qDebug() << "Bindings found " << bindingsCount;
    }
    return qtAndroidBindings;
}

KeyBinder::AndroidInputKeysType KeyBinder::readAndroidInputKeys()
{
    static QMap<QString, QString> androidInputKeys;
    if (androidInputKeys.size() == 0) {
        QFile file(":/input.h");
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            while(!file.atEnd()) {
                QString line = file.readLine();
                if (line.contains("KEY_")) {
                    QStringList keyAndCode = line.split("KEY_")[1].replace("\t", " ").split(" ");
                    QString key = keyAndCode[0];
                    QString code = "";
                    for (int i = 1; i < keyAndCode.length(); i++) {
                        if (keyAndCode.at(i).length() > 0) {
                            code = keyAndCode.at(i).split("\n")[0];
                            break;
                        }
                    }
                    androidInputKeys.insert(key, code);
                }
            }
        }
        file.close();
        qDebug() << "File closed";
    }
    return androidInputKeys;
}

int KeyBinder::bindQtKeys(Qt::Key lowerKey, Qt::Key higherKey,
                          KeyBinder::AndroidInputKeysType androidInputKeys,
                          KeyBinder::QtAndroidBindingKeysType &qtAndroidBindings)
{
    int bindingsCount = 0;
    for (int i = lowerKey; i < higherKey; i++) {
        QString qtKey = QKeySequence(i).toString().toUpper();
        QMap<QString, QString>::iterator it = androidInputKeys.find(qtKey);
        if (it != androidInputKeys.end()) {
            //qDebug() << qtKey;
            qtAndroidBindings.insert(i, it.value());
            bindingsCount++;
        }
    }
    return bindingsCount;
}

int KeyBinder::bindQtKeysCustom(QMap<int, QString> &buttonBindings)
{
    int bindingsCount = 0;
    QMap<int, QString>::iterator it = buttonBindings.find(Qt::Key_Enter);
    if (it != buttonBindings.end()) {
        buttonBindings.insert(QKeySequence(Qt::Key_Return), it.value());
        bindingsCount++;
    }

    return bindingsCount;
}
