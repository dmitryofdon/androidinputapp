#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QString>
#include <QSettings>
#include <QMutex>

class SettingsManager
{
public:
    static QString ADB_GROUP_NAME;
    static QString MOZART_GROUP_NAME;    

    static QString BUILD_PATH_GROUP_NAME;

    static QString ADB_PATH;
    static QString COMEDIA_PATH;
    static QString MOZART_EPG_PATH;
    static QString BOARD_IP;

    static QString EPG_UI_BUILD_PATH;
    static QString AS_BUILD_PATH;

    static QString DEFAULT_EPG_UI_BUILD_PATH;
    static QString DEFAULT_AS_BUILD_PATH;

    SettingsManager(QWidget *parent, QSettings &settings);

    QString readWithDialog(QString group, QString name,
                       bool isAwaysDialog = false);
    QString read(QString group, QString name);
    void writeDefault(QString group, QString name, QString defaultValue);

    QString readAdbPath();
    QString readBoardIp();

private:
    QString callUserDialog(QString name, bool &isOk, QString defValue);
    QString safetyRead(QString group, QString name);
    void safetySet(QString group, QString name, QString value);
    void safetyBeginGroup(QString group);
    void safetyEndGroup();

private:

    QWidget *mParent;
    QSettings &mSettigs;
    QMutex mDataAccess;
};

#endif // SETTINGSMANAGER_H
