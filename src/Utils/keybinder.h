#ifndef KEYBINDER_H
#define KEYBINDER_H

#include <QMap>
#include <QObject>

class KeyBinder
{
public:
    typedef QMap<QString, QString> AndroidInputKeysType;
    typedef QMap<int, QString> QtAndroidBindingKeysType;

    static QtAndroidBindingKeysType bindAndroidInputToQtKeys();

private:
    static KeyBinder::AndroidInputKeysType readAndroidInputKeys();
    static int bindQtKeys(Qt::Key lowerKey, Qt::Key higherKey,
                          KeyBinder::AndroidInputKeysType androidInputKeys,
                          QtAndroidBindingKeysType &qtAndroidBindings);
    static int bindQtKeysCustom(QtAndroidBindingKeysType &qtAndroidBindings);
    KeyBinder();
};

#endif // KEYBINDER_H
