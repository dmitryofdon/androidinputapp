#-------------------------------------------------
#
# Project created by QtCreator 2018-02-26T12:28:15
#
#-------------------------------------------------

QT       += core gui
QT       += concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SkyInput
TEMPLATE = app

#CONFIG += console

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    src/AdbScripts/adbhandler.cpp \
    src/AdbScripts/adbscriptstorage.cpp \
    src/AdbScripts/scriptfile.cpp \
    src/Services/backgroundupdater.cpp \
    src/Utils/keybinder.cpp \
    src/Utils/settingsmanager.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/AdbScripts/skyadbscripts.cpp \
    src/AdbScripts/adbexecscript.cpp \
    src/Services/connectionkeeper.cpp \
    src/Actions/mainmenuaction.cpp \
    src/Actions/helpmenuaction.cpp \
    src/Actions/popupmessage.cpp \
    src/runguard.cpp \
    src/Actions/dialogmanager.cpp \
    src/AdbScripts/internalscriptsstorage.cpp \
    src/AdbScripts/externalscriptsstorage.cpp \
    src/Actions/settingsmenuaction.cpp

HEADERS += \
    src/AdbScripts/adbhandler.h \
    src/AdbScripts/adbscriptstorage.h \
    src/AdbScripts/scriptfile.h \
    src/Services/backgroundupdater.h \    
    src/Utils/keybinder.h \
    src/Utils/settingsmanager.h \
    src/mainwindow.h \
    src/AdbScripts/skyadbscripts.h \
    src/AdbScripts/adbexecscript.h \
    src/Services/connectionkeeper.h \
    src/Actions/mainmenuaction.h \
    src/Actions/helpmenuaction.h \
    src/Actions/popupmessage.h \
    src/runguard.h \
    src/Actions/dialogmanager.h \
    src/AdbScripts/internalscriptsstorage.h \
    src/AdbScripts/externalscriptsstorage.h \
    src/Actions/settingsmenuaction.h

FORMS += \
    res/mainwindow.ui

RESOURCES += \
    res/resources.qrc

SUBDIRS += \
    androidinputapp.pro
