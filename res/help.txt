About:

The application automaticly tries to find adb.
If adb was found it stores it path to the $ADB/ADB_PATH.

You are able to connect usb device also, press F9 and put "USB", then ok.

Key_F1 - Reinstall and start QtRestService
(at $Mozart/COMEDIA_PATH + $BUILD_PATH_GROUP_NAME/AS_BUILD_PATH)
Key_F2 - Reinstall com.sky.epg
(at $Mozart/MOZART_EPG_PATH + $BUILD_PATH_GROUP_NAME/EPG_UI_BUILD_PATH)
Key_F9 - Connect to the board and enable the hdmi
Key_F12 - Reboot board
Key_Pause - Stop/Pause updating background image
Key_Delete - Delete the app by name
Key_Insert - Install the app by the .apk path
Key_CapsLock - Enabling key long pressing
Key_"~" - Input text to the device
