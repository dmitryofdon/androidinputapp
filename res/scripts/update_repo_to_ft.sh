#!/bin/bash
# TODO:
    #0. Add arg to be able to change modes:
        # stash/unstash
        # cleanup
    #1. Force update if branch exists
    #2. Add args to change final touches
    #3. Add arg to cherry-pick commits.

export GERRIT_USER="dubiraylo"
export COMEDIA_DIR="/home/dubiraylo/Projects/Sky_Mozart/comedia/"
export BRANCH_TAG="skyTrial3_"
export GERRIT_PORT="29418"

comedia_ft="42434"
chal_generic_ft="42435"
teatro_ft="42439"
hls_ft="42440"
sky_as_ft="36401"
sky_as_syt="36402"
teatro_syt="42446"

export COMEDIA_PATH="jni/"
export CHAL_PATH="jni/chal/tdal/src"
export TEATRO_PATH="jni/teatro35"
export HLS_PATH="jni/chal-common/libhls"
export SKY_AS_PATH="jni/teatro35/applications/sky-as"

declare -A project_structure=(
    [0]="$COMEDIA_PATH comedia3"
    [1]="$CHAL_PATH chal-android-generic"
    [2]="$TEATRO_PATH teatro35"
    [3]="$HLS_PATH libhls"
    [4]="$SKY_AS_PATH sky-as"
    )

declare -a checkout_commits=($comedia_ft $chal_generic_ft $teatro_ft $hls_ft $sky_as_ft)
declare -a cherry_pick_commits=($sky_as_syt $teatro_syt)

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
VERBOSE_LVL=0

main() {
    parse_arguments "$@"
    print_config

    for commit in "${checkout_commits[@]}"; do
        checkout_to_commit_id $commit
    done

    for commit in "${cherry_pick_commits[@]}"; do
        cherry_pick_commit_id $commit
    done
}

function print_config() {
    print_info "Comedia directory: "${GREEN}$COMEDIA_DIR${NC}
    print_info "Gerrit user: "${GREEN}$GERRIT_USER${NC}
    print_info "Branch tag: "${GREEN}$BRANCH_TAG${NC}
}

INFO_LVL=0
DEBUG_LVL=1
VERBOSE_DEBUG_LVL=2

function print_info() {
    print "$INFO_LVL" "$@"
}
function debug() {
    print "$DEBUG_LVL" "$@"
}
function verbose_debug() {
    print "$VERBOSE_DEBUG_LVL" "$@"
}

function print() {
    lvl=$1
    if [ $VERBOSE_LVL -ge $lvl ]; then
        echo -e "${*:2}"
    fi
}

function die() {
  printf '%s\n' "$1" >&2
  exit 1
}

function retrieve_arg() {
    declare -n res=$1
    if [ "$2" ]; then
        res=$2
    else
        die 'ERROR: '$3' requires a non-empty option argument.'
    fi
}

function parse_arguments() {
    while :; do
        if [[ $1 =~ ^--(.*)=$ ]]; then
            printf 'WARN: requires a non-empty option argument or unknown option: %s\n' "$1" >&2
        else
            case $1 in
                -h|-\?|--help)
                    show_help # Display a usage synopsis.
                    exit
                    ;;
                -d|--dir) # Takes an option argument; ensure it has been specified.
                    retrieve_arg COMEDIA_DIR $2 $1
                    shift;;
                --dir=?*)
                    COMEDIA_DIR=${1#*=};; # Delete everything up to "=" and assign the remainder.
                -v|--verbose)
                    VERBOSE_LVL=$((VERBOSE_LVL + 1));; # Each -v adds 1 to verbosity.
                -u|--user)
                    retrieve_arg GERRIT_USER $2 $1
                    shift;;
                --user=?*)
                    GERRIT_USER=${1#*=};; # Delete everything up to "=" and assign the remainder.
                -t|--tag)
                    retrieve_arg BRANCH_TAG $2 $1
                    shift;;
                --) # End of all options.
                    shift
                    break;;
                -?*)
                    printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2;;
                *) # Default case: No more options, so break out of the loop.
                    break
            esac
        fi
        shift
    done
}

function show_help() {
    printf "Help:
  -v|--verbose          Change a verbosity mode (incremental)
  -d|--dir|--dir=       The comedia path
  -t|--tag|--tag=       A tag name for new branch after uptade
  -u|--user|--user=     A gerrit-username

Please split arguments. Example:
    -v -d "/home/me/comedia" -v

Not supported:
    -dv "/home/me/comedia"
"
}

function path_of_project() {
    project_name=$1
    local -n project_path=$2
    verbose_debug project_name $project_name
    for r in "${project_structure[@]}"; do
        repo_name=$(echo ${r} | cut -d ' ' -f2)
        if [ "$project_name" = "$repo_name" ]; then
            project_path=$COMEDIA_DIR$(echo ${r} | cut -d ' ' -f1)
            return
        fi
    done
}

function commit_id_to_info() {
    commit_id=$1
    info=$(ssh -l $GERRIT_USER -p $GERRIT_PORT gerrit.iwedia.com gerrit query --current-patch-set $commit_id)
    #current_patchset=$(echo $info | grep -oP "currentPatchSet:.*number:.*\d+" | cut -d ' ' -f3)
    project_name=$(echo $info | grep -oP "project:.*\s+.*" | cut -d ' ' -f2)

    path_of_project $project_name project_path

    declare -n array_res=$2
    array_res=($commit_id $project_path $project_name)
}

function export_repo_info() {
    array=(${!1})
    export REPO_ID=${array[0]}
    
    export FOLDER_PATH=${array[1]}
    CURRENT_REPO_NAME=${array[2]}
    GERRIT_REPO=ssh://$GERRIT_USER@gerrit.iwedia.com:$GERRIT_PORT/$CURRENT_REPO_NAME
    GERRIT_CHANGES_ID=${REPO_ID:3:5}
    GERRIT_REF_PATH="refs/changes/$GERRIT_CHANGES_ID/$REPO_ID/"
    export GERRIT_FETCH_PATH="$GERRIT_REPO $GERRIT_REF_PATH"

    cd $FOLDER_PATH

    echo "Exported: dir: $FOLDER_PATH commit: $GERRIT_FETCH_PATH"
    echo ""
}

function fetch_latest_patchset() {
    latest_patch_set $REPO_ID patchset
    $(git fetch $GERRIT_FETCH_PATH$patchset)
    debug "Fetched the latest patchset : $patchset...."
    export LATEST_PATCHSET=$patchset
}

function cherry_pick_commit_id() {
    commit_id=$1
    commit_id_to_info $commit_id info
    debug "Cherry-pick commit info: "${info[@]}
    cherry_pick_commit_info info[@]
}

function cherry_pick_commit_info() {
    export_repo_info $1

    stash_local_changes
    
    fetch_latest_patchset

    res=$(git cherry-pick FETCH_HEAD)
    
    restore_last_stash

    repo_msg "Cherry-picked the commit : $REPO_ID"
}

function latest_patch_set() {
    commit=$1
    declare -n outvar=$2
    outvar=$(ssh -l $GERRIT_USER -p $GERRIT_PORT gerrit.iwedia.com gerrit query --current-patch-set $commit | grep -A1 currentPatchSet | xargs | cut -d ' ' -f3)
    debug "The latest patchset is : $outvar...."
}

function checkout_to_commit_id() {
    commit_id=$1
    commit_id_to_info $commit_id info
    debug "Checkout commit info: "${info[@]}
    checkout_to_commit_info info[@]
}

function checkout_to_commit_info() {
    export_repo_info $1

    repo_msg "Checkouting...."
        
    stash_local_changes

    fetch_latest_patchset
    res=$(git checkout FETCH_HEAD -b $BRANCH_TAG$LATEST_PATCHSET)

    repo_msg "Checkouted on the latest patchset."

    restore_last_stash
}

function stash_local_changes() {
    status_result="$(git status 2>&1 | grep -e "nothing to commit")"
    if [ ${#status_result} -eq 0 ] 
    then
        debug "Stash local changes"
        git stash
        export IS_STASHED=1
    else
        debug "Nothing to stash"
        export IS_STASHED=1
    fi
}

function restore_last_stash() {
    if [ $IS_STASHED -eq 1 ] 
    then
        debug "Restore local stash"
        git stash pop
        export IS_STASHED=0
    fi
}

function repo_msg() {    
    echo ""
    echo -e "************* [ ${RED}$CURRENT_REPO_NAME${NC} ]:${GREEN} $@ ${NC}*************"
    echo ""
}

main "$@"
