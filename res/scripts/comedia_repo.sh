#!/bin/bash
# TODO:
    #1. Force update if branch exists

DEFAULT_IWEDIA_GERRIT_USER="dubiraylo"
DEFAULT_COMEDIA_DIR="/home/dubiraylo/Projects/Sky_Mozart/comedia/"
DEFAULT_BRANCH_TAG="skyTrial3_"
DEFAULT_IWEDIA_GERRIT_PORT="29418"

BRANCH_INC_VERSION=

PREPROCESS_CMD=stash_local_changes
POSTROCESS_CMD=restore_last_stash

export COMEDIA_PATH="jni/"
export CHAL_PATH="jni/chal/tdal/src"
export TEATRO_PATH="jni/teatro35"
export HLS_PATH="jni/chal-common/libhls"
export DASH_PATH="jni/chal-common/libdash"
export SKY_AS_PATH="jni/teatro35/applications/sky-as"

declare -A PROJECT_STRUCTURE=(
    [0]="$COMEDIA_PATH comedia3"
    [1]="$CHAL_PATH chal-android-generic"
    [2]="$TEATRO_PATH teatro35"
    [3]="$HLS_PATH libhls"
    [4]="$SKY_AS_PATH sky-as"
    [5]="$DASH_PATH libdash"
    )

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
VERBOSE_LVL=0
DELIMITER='='

declare -a COMMANDS=()

main() {    
    parse_arguments "$@"
    set_enviroment

    print_project_structure

    cmds_count=${#COMMANDS[@]}
    if [ $cmds_count -eq 0 ]; then
        show_help
    fi
    
    for r in "${COMMANDS[@]}"; do
        print_verbose ${r}
        msg=$(echo ${r} | cut -d $DELIMITER -f1)
        cmd=$(echo ${r} | cut -d $DELIMITER -f2)
        flag=$(echo ${r} | cut -d $DELIMITER -f3)
        print_debug $msg":"$cmd":"$flag
        if [[ "$cmd" =~ ^"once_".*$ ]]; then
            $cmd $flag
        else
            for_each_repo "$msg" $cmd $flag
        fi
    done
}

INFO_LVL=0
DEBUG_LVL=1
VERBOSE_DEBUG_LVL=2

function print_info() {
    print "$INFO_LVL" "$@"
}
function print_debug() {
    print "$DEBUG_LVL" "$@"
}
function print_verbose() {
    print "$VERBOSE_DEBUG_LVL" "$@"
}

function print() {
    lvl=$1
    if [ $VERBOSE_LVL -ge $lvl ]; then
        echo -e "${*:2}"
    fi
}

function die() {
  printf '%s\n' "$1" >&2
  exit 1
}

function retrieve_arg() {
    declare -n _retrieve_arg_res=$1
    if [ "$2" ]; then
        _retrieve_arg_res=$2
    else
        die 'ERROR: '$3' requires a non-empty option argument.'
    fi
}

function export_env_var() {
    name=$1
    def_val=$2
    env_val=$(env | grep -oP ".*$name=.*" | cut -d '=' -f2)
    if [ -z ${env_val} ];
    then
        print_debug "${RED}export $name$=$def_val${NC}"
        export $name=$def_val
    else
        print_debug "${GREEN}env $name=$env_val${NC}"
    fi
}

function set_enviroment() {
    export_env_var "COMEDIA_DIR" $DEFAULT_COMEDIA_DIR
    export_env_var "IWEDIA_GERRIT_USER" $DEFAULT_IWEDIA_GERRIT_USER
    export_env_var "BRANCH_TAG" $DEFAULT_BRANCH_TAG
    export_env_var "IWEDIA_GERRIT_PORT" $DEFAULT_IWEDIA_GERRIT_PORT

    print_debug ""
}

function parse_arguments() {
    while :; do
        if [[ $1 =~ ^--(.*)=$ ]]; then
            printf ${RED}'WARN: requires a non-empty option argument or unknown option: %s\n'${NC} "$1" >&2
            echo ""
        else
            case $1 in
                -h|-\?|--help)
                    show_help # Display a usage synopsis.
                    exit
                    ;;
                -d|--dir) # Takes an option argument; ensure it has been specified.
                    retrieve_arg COMEDIA_DIR $2 $1
                    shift;;
                --dir=?*)
                    COMEDIA_DIR=${1#*=};; # Delete everything up to "=" and assign the remainder.
                -v|--verbose)
                    VERBOSE_LVL=$((VERBOSE_LVL + 1))
                    ;; # Each -v adds 1 to verbosity.
                -u|--user)
                    retrieve_arg IWEDIA_GERRIT_USER $2 $1
                    shift;;
                --user=?*)
                    IWEDIA_GERRIT_USER=${1#*=};; # Delete everything up to "=" and assign the remainder.
                -t|--tag)
                    retrieve_arg BRANCH_TAG $2 $1
                    shift;;
                --tag=?*)
                    BRANCH_TAG=${1#*=};; # Delete everything up to "=" and assign the remainder.
                --log=?*)
                    count=${1#*=}
                    add_cmd "Git log" repo_log "$count"
                    ;;
                -l|--log)
                    count=2
                    add_cmd "Git log" repo_log "$count"
                    ;;
                --status=?*)
                    flag=${1#*=}
                    add_cmd "Git status" repo_status "$flag"                    
                    ;;
                -s|--status)
                    flag=-s
                    add_cmd "Git status" repo_status "$flag"
                    ;;
                -b|--branch)
                    add_cmd "Git branch" repo_branch ""
                    ;;
                --build)
                    add_cmd "Build comedia" once_build_comedia ""
                    ;;
                --rebuild)
                    add_cmd "ReBuild comedia" once_rebuild_comedia ""
                    ;;
                --build_and_install)
                    add_cmd "Build comedia" once_build_and_install_comedia ""
                    ;;
                --foreach=?*)
                    cmd=${1#*=} # Delete everything up to "=" and assign the remainder.                    
                    add_cmd "$cmd" "$cmd" ""
                    ;;
                --cherry-pick=?*)
                    commit_id=${1#*=}
                    add_cmd "cherry-pick:$commit_id" once_cherry_pick "$commit_id"                    
                    ;;
                --checkout=?*)
                    commit_id=${1#*=}
                    add_cmd "checkout:$commit_id" once_checkout "$commit_id"                    
                    ;;
                --no-stash-pop)
                    POSTROCESS_CMD=
                    ;;
                --) # End of all options.
                    shift
                    break;;
                -?*)
                    printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2;;
                *) # Default case: No more options, so break out of the loop.
                    break
            esac
        fi
        shift
    done

    COMEDIA_DIR=$COMEDIA_DIR"/"
}

function add_cmd() {
    msg=$1
    cmd=$2
    args=${*:3}
    fullcmd+=${msg}${DELIMITER}${cmd}${DELIMITER}${args}    
    #echo "Add cmd: [ "$fullcmd" ]"
    COMMANDS+=$fullcmd
}

function show_help() {
    printf "You can export: COMEDIA_DIR, IWEDIA_GERRIT_USER, BRANCH_TAG, IWEDIA_GERRIT_PORT. 
Or the default values will be used:
    COMEDIA_DIR=$DEFAULT_COMEDIA_DIR
    IWEDIA_GERRIT_USER=$DEFAULT_IWEDIA_GERRIT_USER
    BRANCH_TAG=$DEFAULT_BRANCH_TAG
    IWEDIA_GERRIT_PORT=$DEFAULT_IWEDIA_GERRIT_PORT

    Usage:
  -v|--verbose          Change a verbosity mode (incremental).
  -d|--dir|--dir=       The comedia path. Replaces COMEDIA_DIR.
  -t|--tag|--tag=       A tag name for new branch after uptade. Replaces BRANCH_TAG.
  -u|--user|--user=     A gerrit-username. Replaces IWEDIA_GERRIT_USER.
  -l|--log|--log=       The current log of repos. By default: (git log -2) For example: (--log=\'2\')
  -s|--status|--status= The current status of repos. By default: (git status -s) For example: (--status=\'-v\')
  -b|--branch           The current branch of repos. (git branch)
  --foreach=            It runs command for each repo. For example: (--foreach=\'git log\')
  --cherry-pick=        It cherry-picks one or multiple commits for automatically recognized repo. For example: (--cherry-pick=\'36402 42446\').
  --checkout=           It checkouts to one or multiple commits for automatically recognized repo. For example: (--checkout=\'42434 42435 42439 42440 36401\').
  --no-stash-pop        At the end of checkouting or cherry-picking do not pop the latest stash
  --build               It builds the comedia. In specified COMEDIA_DIR.
  --rebuild             It removes libs and obj, runs 'git clean -fxd' and build the comedia. 
  --build_and_install   It builds the comedia and the qrs and installs the qrs on connected board.
"
}

function once_rebuild_comedia() {
    once_cleanup_comedia
    once_build_comedia
}

function once_cleanup_comedia() {
    cd $COMEDIA_DIR
    rm -r -f libs
    rm -r -f obj
    for_each_repo "clean -fxd" repo_clean '-fxd'
    cd -
}


function once_build_comedia() {
    cd $COMEDIA_DIR
    source ./jni/config/envsetup.sh
    iw_select_device sky_mozart
    iw_sky_build_a4tv
    cd -
}

function once_build_and_install_comedia() {
    if [ -z $1 ]
    then
        device=""
    else
        device="-s tcp:"$1
    fi
    once_build_comedia
    cd $COMEDIA_DIR""jni/teatro35/applications/sky-as/QtRESTService
    ./gradlew assembleDebug
    adb $device uninstall com.iwedia.comedia.qtrestservice
    adb $device install -r ./app/build/outputs/apk/debug/app-debug.apk
    adb $device shell am broadcast -a com.iwedia.comedia.qtrestservice.START -n com.iwedia.comedia.qtrestservice/.QTRestServiceReceiver
    adb $device shell 'kill -9 $(pidof com.sky.epg)'
    cd -
}


function repo_name_from_struct() {
    local -n _name_=$1
    _name_=$(echo ${*:2} | cut -d ' ' -f2)
}

function repo_path_from_struct() {
    local -n _path_=$1
    _path_=$COMEDIA_DIR$(echo ${*:2} | cut -d ' ' -f1)
}

function print_project_structure() {
    for r in "${PROJECT_STRUCTURE[@]}"; do
        repo_path_from_struct path ${r}
        repo_name_from_struct name ${r}
        print_verbose "Project: "${RED}$name${NC}"  dir: "${GREEN}$path${NC}
    done
    echo ""
}

function path_of_project() {
    project_name=$1
    local -n _project_path_=$2
    print_verbose project_name $project_name
    for r in "${PROJECT_STRUCTURE[@]}"; do
        repo_name_from_struct repo_name ${r}
        if [ "$project_name" = "$repo_name" ]; then
            repo_path_from_struct _project_path_ ${r}
            return
        fi
    done
}

function for_each_repo() {
    msg=$1
    cmd=$2    
    current_dir=$PWD
    for r in "${PROJECT_STRUCTURE[@]}"; do
        repo_path_from_struct path ${r}
        repo_name_from_struct name ${r}
        cd $path
        CURRENT_REPO_NAME=$name
        repo_msg $msg  
        $cmd ${*:3}
    done
    cd $current_dir
}

function once_checkout() {
    for commit in "$@"; do
        checkout_to_commit_id $commit
    done
}

function once_cherry_pick() {
    for commit in "$@"; do
        cherry_pick_commit_id $commit
    done
}

function repo_clean() {
    git clean -$1
}

function repo_log() {
    git log -$1
}

function repo_status() {
    git status -$1
}

function repo_branch() {
    git branch
}

function commit_current_patchset_info() {
    commit_id=$1
    local -n _commit_current_patchset_info__=$2
    _commit_current_patchset_info__=$(ssh -l $IWEDIA_GERRIT_USER -p $IWEDIA_GERRIT_PORT gerrit.iwedia.com gerrit query --current-patch-set $commit_id)
}

function commit_id_to_info() {
    commit_id=$1
    commit_current_patchset_info $commit_id info
    #current_patchset=$(echo $info | grep -oP "currentPatchSet:.*number:.*\d+" | cut -d ' ' -f3)
    project_name=$(echo $info | grep -oP "project:.*\s+.*" | cut -d ' ' -f2)

    path_of_project $project_name project_path

    declare -n _array_res_=$2
    _array_res_=($commit_id $project_path $project_name)
}

function export_repo_info() {
    array=(${!1})
    export REPO_ID=${array[0]}
    export CURRENT_REPO_NAME=${array[2]}

    repo=ssh://$IWEDIA_GERRIT_USER@gerrit.iwedia.com:$IWEDIA_GERRIT_PORT/$CURRENT_REPO_NAME
    refs_path="refs/changes/${REPO_ID:3:5}/$REPO_ID/"
    export GERRIT_FETCH_PATH="$repo $refs_path"

    path=${array[1]}
    cd $path
    echo "Exported: dir: $path commit: $GERRIT_FETCH_PATH"
    echo ""
}

function check_latest_code() {
    latest_code=$?
    if [ $latest_code -eq 1 ]; then
        echo ""
        echo -e ${RED}"An error happend. Press enter to continue or Ctr-C to stop procedure"${NC}
        echo ""
        read
    fi
}

function fetch_latest_patchset() {
    latest_patch_set $REPO_ID patchset
    git fetch $GERRIT_FETCH_PATH$patchset
    check_latest_code
    print_debug "Fetched the latest patchset : $LATEST_PATCHSET...."    
}

function cherry_pick_commit_id() {
    commit_id=$1
    commit_id_to_info $commit_id info
    print_debug "Cherry-pick commit info: "${info[@]}
    cherry_pick_commit_info info[@]
}

function cherry_pick_commit_info() {
    export_repo_info $1

    $PREPROCESS_CMD

    repo_msg "Cherry-picking...."
    
    fetch_latest_patchset
    git cherry-pick FETCH_HEAD
    check_latest_code

    repo_msg "Cherry-picked the commit : $REPO_ID"

    $POSTROCESS_CMD
}

function latest_patch_set() {
    commit_id=$1
    declare -n _outvar_=$2
    commit_current_patchset_info $commit_id info
    _outvar_=$(echo $info | grep -oP "currentPatchSet:.*number:.*\d+" | cut -d ' ' -f3)
    export LATEST_PATCHSET=$_outvar_
    print_debug "The latest patchset is : $LATEST_PATCHSET...."
}

function checkout_to_commit_id() {
    commit_id=$1
    commit_id_to_info $commit_id info
    print_debug "Checkout commit info: "${info[@]}
    checkout_to_commit_info info[@]
}

function checkout_to_commit_info() {
    export_repo_info $1

    $PREPROCESS_CMD

    repo_msg "Checkouting...."

    fetch_latest_patchset
    git checkout FETCH_HEAD -b $BRANCH_TAG$LATEST_PATCHSET$BRANCH_INC_VERSION
    check_latest_code

    repo_msg "Checkouted on the latest patchset."

    $POSTROCESS_CMD
}

function stash_local_changes() {
    status_result="$(git status 2>&1 | grep -e "nothing to commit")"
    if [ ${#status_result} -eq 0 ] 
    then
        print_debug "Stash local changes"
        git stash
        export IS_STASHED=1
    else
        print_debug "Nothing to stash"
        export IS_STASHED=1
    fi
}

function restore_last_stash() {
    if [ $IS_STASHED -eq 1 ] 
    then
        print_debug "Restore local stash"
        git stash pop
        export IS_STASHED=0
    fi
}

function repo_msg() {    
    echo ""
    echo "---------------------------------------------------------------------------------"
    echo -e " [ ${RED}$CURRENT_REPO_NAME${NC} ] : ${GREEN} $@ ${NC}"
    echo "---------------------------------------------------------------------------------"
    echo ""
}

main "$@"
