#!/bin/bash
cd `dirname $0`

. ./system_main.sh

KEY=$3
KEYX="1a00"$(echo "obase=16; $KEY" | bc)
CODED=$((16#$KEYX))

$ADB_PATH -s $BOARD_IP:5555 shell sendevent /dev/input/event0 4 4 $CODED
$ADB_PATH -s $BOARD_IP:5555 shell sendevent /dev/input/event0 1 $KEY 1
$ADB_PATH -s $BOARD_IP:5555 shell sendevent /dev/input/event0 0 0 0

if [ $4 == '1' ]
then
  sleep 1
fi

$ADB_PATH -s $BOARD_IP:5555 shell sendevent /dev/input/event0 4 4 $CODED
$ADB_PATH -s $BOARD_IP:5555 shell sendevent /dev/input/event0 1 $KEY 0
$ADB_PATH -s $BOARD_IP:5555 shell sendevent /dev/input/event0 0 0 0
